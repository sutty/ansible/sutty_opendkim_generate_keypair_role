Sutty OpenDKIM generate key pairs role
======================================

Generate a 2048b RSA keypair for OpenDKIM and copy them across all
nodes.

Requirements
------------

The host should have the `openssl` binary installed.

Facts
-----

`opendkim_privatekey` the contents of last generated private key.

`opendkim_publickeys` a list of public keys dicts with `selector` and
`publickey` keys.

Role Variables
--------------

Set `dkim_selector` to a dynamic variable based on time, so you can
rotate keys regularly.

Set `group` variable to find the hosts groups.

Set `previous_rotation_age` to a [string recognized by the `age` parameter on the
`find`
module](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/find_module.html#parameter-age)
to rotate DKIM keys that's at least double the rotation period.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_opendkim_generate_keypair"
    vars:
      group: "sutty_dns"
      rotation_age: "4w"
```

License
-------

MIT-Antifa
